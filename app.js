const express = require("express");
const app = express();
const PORT = 3000;

/**
 * This function adds two numbers together
 * @param {number} a first param
 * @param {number} b second param
 * @returns {number}
 */



const add = (a, b) => {
  return a + b;
};

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/numbers", (req, res) => {
  const numbers = add(1, 2);
  res.send(`${numbers}`);
});

app.listen(PORT, () => console.log(`Listening on http://localhost${PORT}`));
