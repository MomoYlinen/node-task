# Node Task

This project contains hello-world app.

## Installation steps

1. Copy repository
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/MomoYlinen/node-task
cd node-task
npm install

```